const Colors = {
    RED: '#D63230',
    LIGHT_RED: '#F4E2E2',
    BLACK: '#000',
    WHITE: '#fff',
    GRAY: '#C0C0C0',
    BLUE: '#4563E4',
    LIGHT_BLUE: '#416475',
    DARK_BLUE: '#023047',
    LIGHT_GRAY: '#8198A3',
    LIGHT_PURPLE: '#6a5acd',
    LIGHT_GREEN: 'rgba(191, 233, 221, 0.25)',
    GREEN: '#00A676'
}

export default Colors