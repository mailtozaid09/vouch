export const headerData = [
    {
        id: 1,
        image: require('../../assets/summary.png'),
        title: 'Summary'
    },
    {
        id: 2,
        image: require('../../assets/withdrawal.png'),
        title: 'Withdrawal'
    },
    {
        id: 3,
        image: require('../../assets/deposits.png'),
        title: 'Deposits'
    },
    {
        id: 2,
        image: require('../../assets/statistics.png'),
        title: 'Statistics'
    },
]


export const transactionData = [
    {
        id: 1,
        title: 'Amazon',
        date: '12 Nov 2021',
        price: '- $32.99',
        letter: 'A'
    },
    {
        id: 2,
        title: 'Amazon',
        date: '12 Nov 2021',
        price: '- $32.99',
        letter: 'A'
    },
    {
        id: 3,
        title: 'Amazon',
        date: '12 Nov 2021',
        price: '- $32.99',
        letter: 'A'
    },
    {
        id: 4,
        title: 'Amazon',
        date: '12 Nov 2021',
        price: '- $32.99',
        letter: 'A'
    },
]


export const friendsData = [
    {
        id: 1,
        title: "Sarah J.",
        price1: '$405',
        price2: '$5000',
        value: 80,
    },
    {
        id: 2,
        title: "Sarah J.",
        price1: '$405',
        price2: '$5000',
        value: 40,
    },
    {
        id: 3,
        title: "Sarah J.",
        price1: '$405',
        price2: '$5000',
        value: 20,
    },
]


export const distributionData = [
    {
        id: 1,
        title: "Weekly Deposit",
        price: '$1000',
        value: 0.82,
        color: '#00A676',
    },
    {
        id: 2,
        title: "1-Time Deposit",
        price: '$5400',
        value: 0.7,
        color: '#4563E4',
    },
    {
        id: 3,
        title: "Smart Actions",
        price: '$50',
        value: 0.15,
        color: '#FFB703',
    },
]

