import React, {useState, useEffect} from 'react';
import {Text, View, Dimensions, StyleSheet, ImageBackground, ScrollView, TouchableOpacity, Image, FlatList} from 'react-native';
import Colors from '../../styles/Colors';
import ProfileHeader from '../../components/header/ProfileHeader';
import Input from '../../components/input/Input';
import Button from '../../components/button/Button';

import { loginUser } from '../../api/api';
import Toast from '../../components/toast/Toast';

const {width} = Dimensions.get('window');

const LoginScreen = ({navigation}) => {

    useEffect(() => {
      
    }, [])

    const [toast, setToast] = useState(false);
    const [toastTitle, setToastTitle] = useState('');
    const [toastEror, setToastError] = useState('');
    const [loader, setLoader] = useState(false);
    const [form, setForm] = useState({})
    const [errors, setErrors] = useState({});
    const [token, setToken] = useState('');

    const onChange = ({name, value}) => {
        setForm({...form, [name]: value})
        setErrors({})
    }


    const loginButton = () => {
        setLoader(true)
        console.log("loginButton");
        console.log(form);
        console.log(form.email);
        console.log(form.password);

        var isEmailValid = false;

        var body =  {
            "email": form.email,
            "password": form.password,
        }

        // var body = {
        //     "email": "eve.holt@reqres.in",
        //     "password": "5cityslicka"        
        // }
        
        if(!form.email){
            console.log("Please enter a valid email");
            setLoader(false)
            setErrors((prev) => {
                return {...prev, email: 'Please enter a valid email'}
            })
        }else{
            isEmailValid = validateEmail(form.email)
            setLoader(false)
        }

        if(!form.password){
            console.log("Please enter a valid password");
            setLoader(false)
            setErrors((prev) => {
                return {...prev, password: 'Please enter a valid password'}
            })
        }

        if(form.email && form.password && isEmailValid){
            console.log("-----Details--");
            setLoader(true)

            loginUser(JSON.stringify(body))
            .then(response => {
                console.log('-----loginUser-----');
                console.log(response);
               

                if(response.error){
                    setToast(true)
                    setToastError('USER NOT FOUND')
                    setLoader(false)
                }

                if(response.token){
                    setToast(true)
                    setToastError('')
                    setToken(response.token)
                    setLoader(false)
                    setToastTitle('LOGIN SUCCESSFULL')

                    setTimeout(() => {
                        navigation.navigate('Dashboard')
                    }, 1500);
                }
              
                setTimeout(() => {
                    setToast(false)
                    setLoader(false)
                }, 2000);
            })
            .catch(error => {
                console.log(error)
                setLoader(false)
                setToastError('LOGIN FAILED')
                setToast(true)
                setTimeout(() => {
                    setToast(false)
                }, 2000);
            });

            
        }
         
        
    }

    const validateEmail = (email) =>  {
        var validRegex = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        
        if (email.match(validRegex)) {
            return true;
        } else {
            
            setErrors((prev) => {
                return {...prev, email: 'Please enter a valid email'}
            })
            return false;
        }
    }

    return (
        <View style={styles.container} >

                <ProfileHeader  />

                <View style={styles.contentContainer} >
                    <View style={styles.imageContainer} >
                        <View style={styles.profileContainer} >
                            <Image 
                                source={require('../../../assets/image.png')}
                                style={styles.profile}
                            />

                            <View style={styles.editContainerWidth} >
                                <View  style={styles.editContainer}>
                                    <Image 
                                        source={require('../../../assets/edit.png')}
                                        style={styles.edit}
                                    />
                                </View>
                            </View>
                        </View>
                        
                    </View>
                   
                    <View style={styles.content} >
                        <ScrollView>
                            <Input
                                label="Email Address"
                                error={errors.email}
                                placeholder="Enter Email Address"
                                clearErrors={() => setErrors({})}
                                onChange={(text) => {onChange({name: 'email', value: text,});}}
                            />

                            <Input
                                label="Password"
                                error={errors.password}
                                secureTextEntry
                                placeholder="Enter Password"
                                clearErrors={() => setErrors({})}
                                onChange={(text) => {onChange({name: 'password', value: text,});}}
                            />

                            <Button
                                title="Login"
                                loader={loader}
                                onPress={() => loginButton()}
                            />
                            
                           
                        </ScrollView>
                       
                    </View>
                    
                    
                </View>
                    {toast
                    ?
                        <View style={styles.toastStyle} >
                            <Toast title={toastTitle} token={token} error={toastEror}  />
                        </View>
                    :
                    null
                    }
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: Colors.BLUE,
    },
    contentContainer: {
        flex: 1,
        padding: 20,
        borderTopLeftRadius: 8,
        borderTopRightRadius: 8,
        backgroundColor: Colors.WHITE,
    },
    toastStyle: {
        marginTop: 50,
        position: 'absolute', 
        bottom: 50,
        width: '100%',
    },
    content: {
        marginTop: 70
    },
    imageContainer: {
        width: width,
        top: -60,
        position: 'absolute',
    },
    profileContainer: {
        width: width,
        alignItems: 'center',
        justifyContent: 'center',
    },
    profile: {
        height: 120,
        width: 120,
        borderRadius: 60,
        resizeMode: 'contain'
    },
    edit: {
        height: 20,
        width: 20,
        resizeMode: 'contain'
    },
    editContainer: {
        height: 40,
        width: 40,
        borderWidth: 1,
        borderColor: Colors.BLUE,
        backgroundColor: Colors.WHITE,
        borderRadius: 20,
        alignItems: 'center',
        justifyContent: 'center',
        marginLeft: 80,
    },
    editContainerWidth: {
        width: '100%',
        alignItems: 'center',
        justifyContent: 'center',
        position: 'absolute',
        bottom: 0,
    },
})

export default LoginScreen