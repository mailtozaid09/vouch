import React, {useState, useEffect} from 'react';
import {Text, View, Dimensions, StyleSheet, ImageBackground, ScrollView, TouchableOpacity, Image, FlatList} from 'react-native';

import { headerData, } from '../../data/data';

import Header from '../../components/header/Header';
import BalanceNTarget from '../../components/cards/BalanceNTarget';


import Colors from '../../styles/Colors';
import TitleCard from '../../components/cards/TitleCard';
import Transactions from '../../components/cards/Transactions';
import ActionsCard from '../../components/cards/ActionsCard';
import FriendsCard from '../../components/cards/FriendsCard';
import Distribution from '../../components/cards/Distribuion';


const {width} = Dimensions.get('window');

const Dashboard = (props) => {

    useEffect(() => {
        
    }, [])

    return (
        <View style={styles.container} >
            <ScrollView>

                <Header data={headerData} />

                <View style={styles.contentContainer} >
                    <BalanceNTarget 
                        balanceTitle='Balance'
                        balanceValue='21,100'
                        targetTitle='Target'
                        targetValue='1,00,000'
                    />

                    <TitleCard
                        title="Investment"
                        subTitle="Savings Distribution"
                    />
                    <Distribution />

                    <TitleCard
                        title="Friends"
                        subTitle="Individual Contribution"
                    />
                    <FriendsCard />

                    <TitleCard
                        title="Active Smart Actions"
                        subTitle="Individual Contribution"
                        addAnother="Add Another"
                    />
                    <ActionsCard 
                        title="Save $5 when I walk 5,000 steps daily"
                        count="48"
                        amount="240"
                        image={require('../../../assets/man.png')}
                    />

                    <TitleCard
                        title="Transactions"
                        viewAllStyle={{color: Colors.BLUE}}
                    />
                    <Transactions />

            </View>
            </ScrollView>
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: Colors.BLUE,
    },
    contentContainer: {
        flex: 1,
        padding: 20,
        borderTopLeftRadius: 8,
        borderTopRightRadius: 8,
        backgroundColor: Colors.WHITE,
    }
})

export default Dashboard