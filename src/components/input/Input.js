import React, {useState, useEffect} from 'react'
import {Text, View, Dimensions, StyleSheet, Image, TextInput, ScrollView , TouchableOpacity } from 'react-native';
import Colors from '../../styles/Colors';

const {width} = Dimensions.get('window');

const Input = (props) => {

    const { label, placeholder, onChange, error, secureTextEntry,} = props; 

    return (
        <View style={styles.container} >
            <Text style={styles.label} >{label}</Text>

            <TextInput 
                placeholder={placeholder}
                onChangeText={onChange}
                style={styles.input}
                secureTextEntry={secureTextEntry ? true : false}
            />

            <Text style={styles.error} >{error ? error : null}</Text>
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
       marginTop: 0
    },
    input: {
        height: 50, 
        marginTop: 8,
        borderRadius: 4,
        paddingLeft: 15,
        fontSize: 16,
        fontWeight: '500',
        fontFamily: 'Poppins-Regular',
        backgroundColor: '#F7F7F7',
    },
    label: {
        fontSize: 16,
        lineHeight: 18,
        marginTop: 8,
        fontWeight: '400',
        fontFamily: 'Poppins-Regular',
        color: '#416475'
    },
    error: {
        fontSize: 14,
        lineHeight: 18,
        marginTop: 2,
        fontFamily: 'Poppins-Regular',
        color: Colors.RED
    },
})


export default Input