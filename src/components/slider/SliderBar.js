import React, {useState, useEffect} from 'react';
import {Text, View, Dimensions, StyleSheet, Image, FlatList} from 'react-native';
import Slider from 'react-native-slider';

import Colors from '../../styles/Colors';
import Divider from '../divider/Divider';

const {width} = Dimensions.get('window');

const SliderBar = (props) => {

    useEffect(() => {

    }, [])
    
    const { value, trackStyle, maximumTrackTintColor, minimumTrackTintColor } = props;

    return (
            <View style={styles.sliderContainer}>
                <Slider
                    value={value}
                    trackStyle={trackStyle}
                    maximumTrackTintColor={maximumTrackTintColor}
                    minimumTrackTintColor={minimumTrackTintColor}
                    thumbTouchSize={{width: 0, height: 0}}
                    thumbStyle={{width: 0, height: 0}}
                />
            </View>
    )
}

const styles = StyleSheet.create({
    sliderContainer: {
        flex: 1,
        
        // marginTop: 10,
        // marginBottom: 10,
        alignItems: 'stretch',
        justifyContent: 'center',
    },
   
})

export default SliderBar