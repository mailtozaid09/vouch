import React, {useState, useEffect} from 'react';
import {Text, View, Dimensions, StyleSheet, Image, FlatList} from 'react-native';


import Colors from '../../styles/Colors';

const {width} = Dimensions.get('window');

const Toast = (props) => {

    useEffect(() => {

    }, [])
    
    const { title, token, error } = props;

    return (
            <View style={styles.toastContainer}>
                <View style={styles.container} >

                    {error
                    
                    ?
                    <Text style={styles.error} >{error}</Text>
                    :
                    <>
                      <Text style={styles.token} >{title}</Text>
                    <Text style={styles.title} >{token}</Text>
                    </>
                    }
                  
                </View>
            </View>
    )
}

const styles = StyleSheet.create({
    toastContainer: {
        flex: 1,
        padding: 10,
        alignItems: 'center',
        alignItems: 'center',
        justifyContent: 'center',
    },
    container: {
        width: '80%',
        padding: 0,
        borderRadius: 8,
        backgroundColor: '#F7F7F7',
        alignItems: 'center',
        borderWidth: 1,
        borderColor: Colors.GRAY,
        justifyContent: 'center',
    },
    error: {
        fontSize: 18,
        lineHeight: 22,
        marginTop: 8,
        padding: 10,
        color:  Colors.RED,
        fontFamily: 'Poppins-SemiBold',
    },
    token: {
        fontSize: 18,
        lineHeight: 22,
        marginTop: 8,
        color:  Colors.DARK_BLUE,
        fontFamily: 'Poppins-SemiBold',
    },
    title: {
        fontSize: 14,
        lineHeight: 18,
        marginTop: 4,
        marginBottom: 4,
        color:  Colors.GRAY,
        fontFamily: 'Poppins-Regular',
    }
})

export default Toast