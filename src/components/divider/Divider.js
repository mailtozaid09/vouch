import React, {useState, useEffect} from 'react';
import {Text, View, Dimensions, StyleSheet, Image, FlatList} from 'react-native';

import Colors from '../../styles/Colors';

const {width} = Dimensions.get('window');

const Divider = (props) => {

    useEffect(() => {

    }, [])
    
    const { horizontal, dividerStyle } = props;

    return (
        <View style={[styles.container, {dividerStyle}]} >
            <View style={horizontal ? styles.horizontalDivider : styles.VerticalDivider} />
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        backgroundColor: Colors.LIGHT_GRAY
    },
    horizontalDivider: {
        height: 30,
        width: 2,
    },
    VerticalDivider: {

    }
})

export default Divider