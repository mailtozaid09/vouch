import React, {useState, useEffect} from 'react';
import {Text, View, Dimensions, StyleSheet, Image, FlatList} from 'react-native';

import Colors from '../../styles/Colors';
import { distributionData } from '../../data/data';
import Divider from '../divider/Divider';
import SliderBar from '../slider/SliderBar';


const {width} = Dimensions.get('window');

const Distribution = (props) => {

    useEffect(() => {

    }, [])
    
    const { title, subTitle, addAnother, viewAllStyle } = props;

    return (
        <View style={styles.container} >
           <View>
                <FlatList
                    data={distributionData}
                    keyExtractor={(item) => item.id}
                    renderItem={({item}) => {
                        return(
                            <>
                            <View style={styles.itemContainer} >
                                <View style={styles.textContainer} >
                                    <View style={{ width: 130}} >
                                        <Text style={styles.subTitle} >{item.title}</Text>
                                        <Text style={styles.title} >{item.price}</Text>
                                    </View>
                                </View>
                                <View style={styles.sliderContainer} >
                                    <SliderBar 
                                        value={item.value}
                                        trackStyle={{height: 15, borderRadius: 10}}
                                        maximumTrackTintColor={Colors.LIGHT_GRAY}
                                        minimumTrackTintColor={item.color}
                                    />
                                </View>
                            </View>
                            </>
                        )
                    }}
                />
            </View>
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
    },
    itemContainer: {
        height: 60,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
    },
    divider: {
        height: 1,
        marginLeft: 15,
        marginRight: 15,
        backgroundColor: Colors.GRAY
    },
    textContainer: {
        flexDirection: 'row',
        alignItems: 'center',
    },
    sliderContainer: {
        flexDirection: 'row',
        alignItems: 'center',
        marginLeft: 10,
        flex: 1,
        width: '100%',
    },
    letterContainer: {
        justifyContent: 'center',
        alignItems: 'center',
        height: 50, 
        width: 50,
        borderRadius: 25,
        marginRight: 15,
        backgroundColor: Colors.LIGHT_GREEN,
    },
    letter: {
        
        color: Colors.GREEN,
        fontSize: 22,
    },
    title: {
        fontSize: 16,
        color: Colors.DARK_BLUE,
        fontFamily: 'Poppins-SemiBold',
        lineHeight: 24,
    },
    subTitle: {
        fontSize: 14,
        color: Colors.LIGHT_GRAY,
        fontFamily: 'Poppins-Regular',
        lineHeight: 24,
    },
    price: {
        fontSize: 16,
        color: Colors.RED,
        fontFamily: 'Poppins-SemiBold',
        lineHeight: 24,
    }
})

export default Distribution