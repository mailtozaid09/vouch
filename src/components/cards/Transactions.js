import React, {useState, useEffect} from 'react';
import {Text, View, Dimensions, StyleSheet, Image, FlatList} from 'react-native';

import Colors from '../../styles/Colors';
import { transactionData } from '../../data/data';
import Divider from '../divider/Divider';


const {width} = Dimensions.get('window');

const Transactions = (props) => {

    useEffect(() => {

    }, [])
    
    const { title, subTitle, addAnother, viewAllStyle } = props;

    return (
        <View style={styles.container} >
           <View>
                <FlatList
                    data={transactionData}
                    keyExtractor={(item) => item.id}
                    renderItem={({item}) => {
                        return(
                            <>
                            <View style={styles.itemContainer} >
                                <View style={styles.textContainer} >
                                    <View style={styles.letterContainer} >
                                        <Text style={styles.letter} >{item.letter}</Text>
                                    </View>
                                    <View>
                                        <Text style={styles.title} >{item.title}</Text>
                                        <Text style={styles.subTitle} >{item.date}</Text>
                                    </View>
                                </View>
                                <View style={styles.textContainer} >
                                    <Text style={[styles.price]} >{item.price}</Text> 
                                </View>
                            </View>
                            <View style={styles.divider} />
                            </>
                        )
                    }}
                />
            </View>
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        backgroundColor: Colors.WHITE,
        borderRadius: 8,
        marginTop: 20,
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 6,
        },
        shadowOpacity: 0.37,
        shadowRadius: 7.49,
        
        elevation: 12,
    },
    itemContainer: {
        flexDirection: 'row',
        alignItems: 'center',
        padding: 15,
        paddingTop: 10,
        paddingBottom: 10,
        justifyContent: 'space-between',
    },
    divider: {
        height: 1,
        marginLeft: 15,
        marginRight: 15,
        backgroundColor: Colors.GRAY
    },
    textContainer: {
        flexDirection: 'row',
        alignItems: 'center',
    },
    letterContainer: {
        justifyContent: 'center',
        alignItems: 'center',
        height: 50, 
        width: 50,
        borderRadius: 25,
        marginRight: 15,
        backgroundColor: Colors.LIGHT_GREEN,
    },
    letter: {
        
        color: Colors.GREEN,
        fontSize: 22,
    },
    title: {
        fontSize: 16,
        color: Colors.DARK_BLUE,
        fontFamily: 'Poppins-SemiBold',
        lineHeight: 24,
    },
    subTitle: {
        fontSize: 14,
        color: Colors.LIGHT_GRAY,
        fontFamily: 'Poppins-Regular',
        lineHeight: 24,
    },
    price: {
        fontSize: 16,
        color: Colors.RED,
        fontFamily: 'Poppins-SemiBold',
        lineHeight: 24,
    }
})

export default Transactions