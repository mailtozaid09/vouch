import React, {useState, useEffect} from 'react';
import {Text, View, Dimensions, StyleSheet, Image, FlatList} from 'react-native';

import Colors from '../../styles/Colors';
import { transactionData } from '../../data/data';
import Divider from '../divider/Divider';


const {width} = Dimensions.get('window');

const ActionsCard = (props) => {

    useEffect(() => {

    }, [])
    
    const { title, amount, count, image } = props;

    return (
        <View style={styles.container} >
            <View style={styles.cardContainer} >
                <View style={styles.textContainer} >
                    <View style={styles.titleContainer} >
                        <Text style={styles.title}>Save <Text style={[styles.title, {textDecorationLine: 'underline'}]}>$5</Text> when I walk  <Text style={[styles.title, {textDecorationLine: 'underline',}]}>5,000 </Text> steps daily </Text>
                    </View>
                    <View style={styles.amountCount} >
                        <View style={styles.amountContainer} >
                            <Text style={styles.countTitle}>Counts</Text>   
                            <Text style={styles.countSubTitle}>{count} times</Text>   
                        </View>
                        <View style={styles.countContainer} >
                            <Text style={styles.countTitle}>Amount</Text>   
                            <Text style={styles.countSubTitle}>$ {amount}</Text>   
                        </View>
                    </View>
                </View>
                <View style={styles.imageContainer} >
                    <Image source={image} style={styles.image} />
                </View>
            </View>
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        backgroundColor: Colors.LIGHT_RED,
        borderRadius: 8,
        marginTop: 20,
        marginBottom:10,
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.37,
        shadowRadius: 2.49,
        elevation: 4,
    },
    textContainer: {
        flex: 1,
        height: 200,
        padding: 10,
        justifyContent: 'space-between'
    },
    titleContainer: {
    },
    amountCount: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
    },
    amountContainer: {

    },
    countContainer: {

    },
    cardContainer: {
        padding: 15,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
    },  
    image: {
        height: 200,
        width: 100,
        resizeMode: 'contain'
    },
    title: {
        fontSize: 22,
        color: Colors.DARK_BLUE,
        fontFamily: 'Poppins-Bold',
        lineHeight: 26,
    },
    countTitle: {
        fontSize: 14,
        color: Colors.LIGHT_GRAY,
        fontFamily: 'Poppins-Regular',
        lineHeight: 24,
    },
    countSubTitle: {
        fontSize: 16,
        color: Colors.DARK_BLUE,
        fontFamily: 'Poppins-SemiBold',
        lineHeight: 24,
    }
})

export default ActionsCard