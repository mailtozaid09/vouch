import React, {useState, useEffect} from 'react';
import {Text, View, Dimensions, StyleSheet, Image, FlatList} from 'react-native';

import Colors from '../../styles/Colors';

const {width} = Dimensions.get('window');

const TitleCard = (props) => {

    useEffect(() => {

    }, [])
    
    const { title, subTitle, addAnother, viewAllStyle } = props;

    return (
        <View style={styles.container} >
            <View style={styles.textContainer} >
                <Text style={styles.title} >{title}</Text>
               {subTitle ? <Text style={styles.subTitle} >{subTitle}</Text> : null} 
            </View>
            <View style={styles.textContainer} >
                {addAnother ? <Text style={[styles.viewAll]} >+ {addAnother}</Text> : <Text style={[styles.viewAll, viewAllStyle]} >View All</Text>}
                
            </View>
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        flexDirection: 'row',
        alignItems: 'center',
        marginTop: 20,
        justifyContent: 'space-between'
    },
    title: {
        fontSize: 16,
        color: Colors.DARK_BLUE,
        fontFamily: 'Poppins-SemiBold',
        lineHeight: 24,
    },
    subTitle: {
        fontSize: 14,
        color: Colors.LIGHT_GRAY,
        fontFamily: 'Poppins-Regular',
        lineHeight: 24,
    },
    viewAll: {
        fontSize: 14,
        color: Colors.LIGHT_GRAY,
        fontFamily: 'Poppins-Regular',
        lineHeight: 24,
    }
})

export default TitleCard