import React, {useState, useEffect} from 'react';
import {Text, View, Dimensions, StyleSheet, Image, FlatList} from 'react-native';
import Slider from 'react-native-slider';

import Colors from '../../styles/Colors';
import Divider from '../divider/Divider';
import SliderBar from '../slider/SliderBar';

const {width} = Dimensions.get('window');

const BalanceNTarget = (props) => {

    useEffect(() => {

    }, [])
    
    const { balanceTitle, balanceValue,  targetTitle, targetValue } = props;

    return (
        <View style={styles.container} >
            <View style={styles.rowSpace} >
                <View style={styles.rowSpace}>
                    <View>
                        <View>
                            <Image 
                                source={require('../../../assets/balance.png')}
                                style={styles.icon}
                            />
                        </View>
                    </View>
                    <View style={styles.textContainer} >
                        <Text style={styles.title}>{balanceTitle}</Text>
                        <Text style={styles.value}>{'\u0024'}{balanceValue}</Text>
                    </View>
                </View>

                <View>
                    <Divider horizontal />
                </View>

                <View style={styles.rowSpace}>
                    <View>
                        <View>
                            <Image 
                                source={require('../../../assets/balance.png')}
                                style={styles.icon}
                            />
                        </View>
                    </View>
                    <View style={styles.textContainer} >
                        <Text style={styles.title}>{targetTitle}</Text>
                        <Text style={styles.value}>{'\u0024'}{targetValue}</Text>
                    </View>
                </View>
            </View>


            <SliderBar 
                value={0.8}
                trackStyle={{height: 5, borderRadius: 10}}
                maximumTrackTintColor={Colors.LIGHT_GRAY}
                minimumTrackTintColor={Colors.GREEN}
            />
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        //padding: 20,
    },
    sliderContainer: {
        flex: 1,
        
        marginTop: 20,
        marginBottom: 20,
        alignItems: 'stretch',
        justifyContent: 'center',
    },
    content: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between'
    },
    rowSpace: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between'
    },
    textContainer: {
        marginLeft: 10,
    },
    icon: {
        height: 30,
        width: 30,
        resizeMode: 'contain'
    },
    title: {
        fontSize: 14,
        color: Colors.LIGHT_BLUE,
        fontFamily: 'Poppins-Regular',
        lineHeight: 18,
        textAlign: 'left',
    },
    value: {
        fontSize: 16,
        color: Colors.DARK_BLUE,
        fontFamily: 'Poppins-Bold',
        lineHeight: 24,
        marginTop: 4,
        textAlign: 'left',
    },
})

export default BalanceNTarget