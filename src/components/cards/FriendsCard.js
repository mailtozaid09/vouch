import React, {useState, useEffect} from 'react';
import {Text, View, Dimensions, StyleSheet, Image, FlatList} from 'react-native';

import Colors from '../../styles/Colors';
import { friendsData } from '../../data/data';

import ArcSlider from "rn-arc-slider";


const {width} = Dimensions.get('window');



const FriendsCard = (props) => {

    useEffect(() => {

    }, [])
    
    
    const { title, subTitle, addAnother, viewAllStyle } = props;

    return (
        <View style={styles.container} >
            <View style={styles.textContainer} >
                <FlatList
                    data={friendsData}
                    contentContainerStyle={{flex: 1, flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between'}}
                    keyExtractor={(item) => item.id}
                    renderItem={({item}) => {
                        return(
                            <View style={styles.itemContainer} >
                                                
                                <ArcSlider
                                    value={item.value}
                                    trackColor={Colors.BLUE}
                                    noThumb={true}
                                    trackRadius={20}
                                    trackWidth={4}
                                />
                                <View style={{position: 'absolute', top: 24, left: 24}} >
                                    <Image source={require('../../../assets/user.png')} style={styles.user} />
                                </View>
                               
                                <Text style={styles.title} >{item.title}</Text>
                                <Text style={styles.subTitle} >{item.price1} / {item.price2}</Text>
                            </View>
                        )
                    }}
                />
            </View>

           
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        flexDirection: 'row',
        alignItems: 'center',
        marginTop: 10,
        justifyContent: 'space-between'
    },
    textContainer: {
        width: '100%',
    },
    title: {
        fontSize: 16,
        color: Colors.DARK_BLUE,
        fontFamily: 'Poppins-SemiBold',
        lineHeight: 24,
    },
    subTitle: {
        fontSize: 12,
        color: Colors.DARK_BLUE,
        fontFamily: 'Poppins-Regular',
        lineHeight: 18,
    },
    itemContainer: {
        backgroundColor: Colors.WHITE,
        borderRadius: 8,
        margin: 4,
        width: 90,
        padding: 10,
        shadowColor: "#000",
        shadowOffset: {
            width: 2,
            height: 3,
        },
        shadowOpacity: 0.37,
        shadowRadius: 7.49,
        
        elevation: 12,
    },
    user: {
        height: 36,
        width: 36,
        borderRadius: 18,
    }
})

export default FriendsCard