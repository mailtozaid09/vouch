import React, {useState, useEffect} from 'react';
import {Text, View, Dimensions, StyleSheet, Image, FlatList} from 'react-native';

import Colors from '../../styles/Colors';

const {width} = Dimensions.get('window');

const Header = (props) => {

    useEffect(() => {

    }, [])
    
    const { data } = props;

    return (
        <View style={styles.container} >
            <View style={styles.header} >
                <View style={styles.textContainer} >
                    <Text style={styles.text}>Your ideal deposit amount needs to be <Text style={styles.textBold} >54.79</Text> for <Text  style={styles.textBold} >1825 days.</Text> </Text>
                </View>
                <View>
                    <Image 
                        source={require('../../../assets/header.png')}
                        style={styles.headerImg}
                    />
                </View>
            </View>

            <View>
                <FlatList
                    data={data}
                    contentContainerStyle={styles.flalistContainer}
                    keyExtractor={(item) => item.id}
                    renderItem={({item}) => {
                        return(
                            <View style={styles.itemContainer} >
                                <View>
                                    <Image 
                                        source={item.image}
                                        style={styles.icon}
                                    />
                                </View>
                                <Text style={styles.title} >{item.title}</Text>
                            </View>
                        )
                    }}
                />
            </View>
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        padding: 20,
    },
    header: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between'
    },
    headerImg: {
        height: 100,
        width: width*0.4,
        resizeMode: 'contain',
    },
    textContainer: {
        width: width*0.4,
    },
    text: {
        fontSize: 14,
        color: Colors.WHITE,
        fontFamily: 'Poppins-Regular',
        lineHeight: 20,
        textAlign: 'left',
    },
    textBold: {
        fontFamily: 'Poppins-Bold',
    },
    icon: {
        height: 30,
        width: 30,
        resizeMode: 'contain'
    },
    title: {
        fontSize: 12,
        color: Colors.WHITE,
        fontFamily: 'Poppins-Regular',
        lineHeight: 18,
        marginTop: 4,
        textAlign: 'left',
    },
    flalistContainer: {
        flexDirection: 'row',
        justifyContent: 'space-between'
    },
    itemContainer: {
        alignItems: 'center',
        marginTop: 20,
    }
})

export default Header