import React from 'react'
import { Text, View, StyleSheet, ScrollView, Keyboard, TouchableOpacity, ActivityIndicator, Image, Dimensions,  } from 'react-native'
import Colors from '../../styles/Colors';

const Button = (props) => {

    const { title, onPress, loader } = props;
    return (
        <>
        {loader
        ?
        <View style={styles.container} >
            <ActivityIndicator size='large' color={Colors.WHITE} />
        </View>
        :
        <TouchableOpacity activeOpacity={0.5} onPress={onPress} style={styles.container} >
            <Text style={styles.title} >{title}</Text>
        </TouchableOpacity>
        }
         </>
    )
}

const styles = StyleSheet.create({
    container: {
        marginTop: 20,
        padding: 10,
        height: 50,
        borderRadius: 40,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: Colors.BLUE
    },
    title: {
        fontSize: 18,
        fontWeight: '600',
        fontFamily: 'Poppins-Regular',
        color: Colors.WHITE
    }
})

export default Button