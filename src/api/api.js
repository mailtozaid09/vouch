const baseUrl = 'https://reqres.in/api/login'

// Get Movies 
export function loginUser(body) {
    return(
        fetch(
            baseUrl,
            {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json',
                },
                body: body
            }
        )
        .then(res => res.json())
    );
}