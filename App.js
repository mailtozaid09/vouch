import 'react-native-gesture-handler';
import * as React from 'react';
import { LogBox, StatusBar, Text, View, Image,  } from 'react-native'
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';

import Dashboard from './src/screens/dashboard/Dashboard';
import LoginScreen from './src/screens/login/LoginScreen';

import Colors from './src/styles/Colors';

LogBox.ignoreAllLogs(true);

const Stack = createStackNavigator();

const App = () => {
    return (
        <NavigationContainer>
            <StatusBar backgroundColor={Colors.BLACK} />
            <Stack.Navigator>
            <Stack.Screen 
                    name="Login" 
                    component={LoginScreen}
                    options={{
                        headerShown: false
                    }}
                />
                <Stack.Screen
                    name="Dashboard"
                    component={Dashboard}
                    options={{
                        title: 'Account',
                        headerStyle: {
                            elevation: 0,
                            shadowOpacity: 0,
                          backgroundColor: Colors.BLUE,
                        },
                        headerTintColor: '#fff',
                        headerTitleStyle: {
                          fontWeight: 'bold',
                        },
                        headerLeft: () => (
                            <Image source={require('./assets/back.png')} style={{height: 22, width: 22, resizeMode: 'contain', margin: 10}} />
                        ),
                        headerRight: () => (
                            <Image source={require('./assets/notification.png')} style={{height: 22, width: 22, resizeMode: 'contain', margin: 10}} />
                        ),
                    }}
                />
                
            </Stack.Navigator>
        </NavigationContainer>
    );
};

export default App;